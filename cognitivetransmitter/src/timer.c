#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "timer.h"

timer_Callback g_callback_timer;

void initTimer0(TIMER_TYPE_T eType, uint32_t prescalar, uint32_t tc_count,timer_Callback callback)
{
//	printf("init timer\n");
	LPC_SC->PCONP |= (1<<eType);

	switch(eType)
	{
		case timer0:
		{
			LPC_SC->PCLKSEL0 &= ~(3<<2);
			LPC_SC->PCLKSEL0 |= (1<<2);
		}
		break;
		case timer1:
		{
			LPC_SC->PCLKSEL0 &= ~(3<<4);
			LPC_SC->PCLKSEL0 |= (1<<4);
			break;
		}
	}

	//timer 0
	//LPC_PINCON->PINSEL0 |= (0xff <<12);

	//set prescale
	LPC_TIM0->PR = prescalar;

	//match control // set interrupt and tc reset
	LPC_TIM0->MCR &= ~((uint32_t)0x7);
	LPC_TIM0->MCR = 0x3;
	//set match
	LPC_TIM0->MR0 = tc_count;

	//hook up call back
	g_callback_timer = callback;
	LPC_TIM0->TCR |=(1<<1);
	NVIC_EnableIRQ(TIMER0_IRQn);


}

TIMER0_IRQHandler()
{
	//printf("interrupt handler\n");
	if(LPC_TIM0->IR & (1<<0))
	{
		LPC_TIM0->PC =0;
		LPC_TIM0->IR = 0x1<<0;
		//LPC_TIM0->TC = 0;
		g_callback_timer();

	//	puts("i");
	}
	else
	{
		printf("****");
	}
}

void enableTimer0()
{
	LPC_TIM0->PC = 0;
	LPC_TIM0->TC = 0;
	LPC_TIM0->IR = 0xFF;
	LPC_TIM0->TCR = 2;
	LPC_TIM0->TCR = 1;
}

void disableTimer()
{
	LPC_TIM0->TCR  &= (~1);
	LPC_TIM0->TCR =2;
	NVIC_DisableIRQ(TIMER0_IRQn);
}
