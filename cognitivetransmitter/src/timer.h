#ifndef __TIMER__H
#define __TIMER__H

typedef enum TIMER_TYPE_T
{
	timer0=1,
	timer1=2,
	timer2=22,
	timer3=23

} TIMER_TYPE_T;

typedef void (*timer_Callback)(void);

void initTimer0(TIMER_TYPE_T eType, uint32_t prescalar, uint32_t tc_count,timer_Callback callback);
void enableTimer0();
void disableTimer();

#endif
