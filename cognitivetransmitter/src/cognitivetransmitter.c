/*
===============================================================================
 Name        : cognitivetransmitter.c
 Author      : Bharat Khanna
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
#define Identity 8
#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#include <stdio.h>
#include "timer.h"

#define lisa 16
#define BUFF_SIZE 18
#define SIZE BUFF_SIZE*8
#define scramOrder 5

#define SizeOfLbcColumn 12
#define paritySize (8*(SizeOfLbcColumn-8))
// TODO: insert other include files here

// TODO: insert other definitions and declarations here

unsigned char tx;
unsigned char status =0;

/*void delay(int ms)
{
	int multiplier = 24000,count;
	for ( count = multiplier*ms ; count >0  ; count--);

}
*/

void delay(uint32_t delayInMs)
{
	LPC_TIM0->TCR = 0x02;		/* reset timer */
	LPC_TIM0->PR  = 0x00;		/* set prescaler to zero */
	LPC_TIM0->MR0 = delayInMs * (9000000 / 1000-1);
	LPC_TIM0->IR  = 0xff;		/* reset all interrrupts */
	LPC_TIM0->MCR = 0x04;		/* stop timer on match */
	LPC_TIM0->TCR = 0x01;		/* start timer */

	/* wait until delay time has elapsed */
	while (LPC_TIM0->TCR & 0x01);

	return;
}


void callback_timer(void)
{
	//printf("%d\n",tx);
	status = 0;

}

void GPIOinitOut(uint8_t portNum, uint32_t pinNum)			// Initilize output ports and pins
{
	if (portNum == 0)
	{
		LPC_GPIO0->FIODIR |= (1 << pinNum); //Initilize port 0
	}
	else if (portNum == 1)
	{
	LPC_GPIO1->FIODIR |= (1 << pinNum); //Initilize port 1
	}
	else if (portNum == 2)
	{
	LPC_GPIO2->FIODIR |= (1 << pinNum); //Initilize port 2
	}
	else
	{
		puts("undefined port\n");
	}
}

void GPIOinitIN(uint8_t portNum, uint32_t pinNum)			// initilize input ports and pin
{
	if (portNum == 0)
	{
		LPC_GPIO0->FIODIR &= ~(1 << pinNum);  //Initilize port 0
	}
	else if (portNum == 1)
	{
		LPC_GPIO1->FIODIR &= ~(1 << pinNum);  //Initilize port 1
	}
	else if (portNum == 0)
	{
		LPC_GPIO0->FIODIR &= ~(1 << pinNum);  //Initilize port 2
	}
	else
	{
		puts("undefined port\n\n");
	}
}


void setGPIO(uint8_t portNum, uint32_t pinNum)		// set GPIO when set output High
{
	if (portNum == 0)
	{
		LPC_GPIO0->FIOSET = (1 << pinNum);  //set port 0
	}
	else if (portNum == 1)
	{
		LPC_GPIO1->FIOSET = (1 << pinNum); //set port 1
	}
	else if (portNum == 2)
	{
		LPC_GPIO2->FIOSET = (1 << pinNum); //set port 2
	}
	else
	{
		puts("undefined port\n");
	}
}


void clearGPIO(uint8_t portNum, uint32_t pinNum)    // clear GPIO when clear output Low
{
	if (portNum == 0)
	{
		LPC_GPIO0->FIOCLR = (1 << pinNum); //clear port 0
	}
	else if (portNum == 1)
	{
		LPC_GPIO1->FIOCLR = (1 << pinNum); //clear port 1
	}
	else if (portNum == 2)
	{
		LPC_GPIO2->FIOCLR = (1 << pinNum); //clear port 2
	}
	else
	{
		puts("undefined port\n");
	}
}


int xor(int a, int b)   //xor function
{
	if (a == b)  // a = 0 , b = 0,1,0,1,1,0,0,1,1,1,1,1,0,1,0,1,0,0,0,0,1,0,0,1,0,0,1,0,0,0,1,1,1,1,1,0,1,1,0,1,0,0,1,0,1,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,1,0,0,0,0,1,1,1,0,1,1,0,0,1,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,1,0,1,1,0,1,0,1,0,0,0,0,1,0,0,1,0,0,1,0,1,0,1,1,1,0,0,1,1,0,1,1,0,0,0,0,0,1,1,0,0,1,0,1,1,1,1,1,0,1,0,0,1,0,1,0,0,0,0,0,1,1,0,1,1,0,0,1,0,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,1,0,0,1,1,0,1,0,1,0,0,0,0,0,0,1,1,1,0,1,1,1,1,0,0,1,0,0,1,1,0,0,1,0,0,0,1 or a = 1, b = 1
	{
		return 0;
	}
	else if (a == 1)  //a = 1, b = 0
	{
		return 1;
	}
	else if (b == 1)  // a = 0, b = 1
	{
		return 1;
	}
	else
	{
		return 0;
	}
}




int main(void) {

	int i ,j;
	//unsigned char sync[lisa]={0x5A,0XAA,0xAA,0xAA,0XAA,0xAA,0xAA,0XAA,0xAA,0xA9,0XAA,0xAB,0xAC,0XAD,0xAE,0xAF};
	unsigned char sync[lisa]={0xA0,0XA1,0xA2,0xA3,0XA4,0xA5,0xA6,0XA7,0xA8,0xA9,0XAA,0xAB,0xAC,0XAD,0xAE,0xAF};
	unsigned char hex[BUFF_SIZE] = {0xA0,0x48,0x65,0x6C, 0x6C,0x6F,0x2A,0x42, 0x68, 0x61, 0x72, 0x61, 0x74, 0x2A,0x39, 0x32, 0x35, 0x36}; //Hello*Bharat*9256
	//unsigned char hex[BUFF_SIZE] =  {0xA0,0x48,0x65,0x6C, 0x6C,0x6F,0x2A,0x54, 0x69,0x61,0x6E,0x72,0x61,0x6E,0x2A,0x35,0x30,0x34,0x31}; //Hello*Tianran*5041
	//unsigned char hex[BUFF_SIZE] = {0xA0,0x42, 0x68, 0x61, 0x72, 0x61, 0x74, 0x2A,0x39, 0x32, 0x35, 0x36}; //Hello*Bharat*9256
	GPIOinitOut(0, 2);
	unsigned int bi[SIZE] = {0};
	unsigned int scrambled[SIZE] = {0};
	int k=0, temp;
	unsigned int d1, d2, d3, d4, d5,d6,d7,d8,d9,d10,d11,d12;

	int C[BUFF_SIZE*12]={0};
    for (i = 0; i < BUFF_SIZE; i++)
    	{
    		for (j = 7; j >= 0; j--)		// Hex to binary conversion
    		{
    			if (((hex[i] >> j) & 1) != 0)
    			{
    				bi[k] = 1;
    			}
    			else
    			{
    				bi[k] = 0;
    			}
    			k++;
    		}
    	}
    /*
    								printf("Payload \n");
    								for (k = 0; k <SIZE; k++)
    								{
    									printf("%x", bi[k]);
    								}
    */
	//Set Scrambler


    if(scramOrder == 5)
    {
     d1 = 0;
	d2 = 0;
	d3 = 0;
	d4 = 0;
	d5 = 0;
	for (i = 0; i < SIZE; i++)  //loop to
		{
			temp = xor(xor(d3, d5), bi[i]);
			d5 = d4;
			d4 = d3;
			d3 = d2;
			d2 = d1;
			d1 = temp;
			scrambled[i] = temp;    //store values in output array
	}
    }

    if(scramOrder == 7)
    {
        d1 = 0;
    	d2 = 0;
    	d3 = 0;
    	d4 = 0;
    	d5 = 0;
    	d6 = 0;
    	d7 = 0;
    	for (i = 0; i < SIZE; i++)  //loop to
    		{
    			temp = xor(xor(d4, d7), bi[i]);

    			d7 = d6;
    			d6 = d5;
    			d5 = d4;
    			d4 = d3;
    			d3 = d2;
    			d2 = d1;
    			d1 = temp;
    			scrambled[i] = temp;

    		}
    }


    if(scramOrder == 9)
        {
            d1 = 0;
        	d2 = 0;
        	d3 = 0;
        	d4 = 0;
        	d5 = 0;
        	d6 = 0;
        	d7 = 0;
        	d8 = 0;
        	d9 = 0;
        	for (i = 0; i < SIZE; i++)  //loop to
        		{
        			temp = xor(xor(d5, d9), bi[i]);
        			d9 = d8;
        			d8 = d7;
        			d7 = d6;
        			d6 = d5;
        			d5 = d4;
        			d4 = d3;
        			d3 = d2;
        			d2 = d1;
        			d1 = temp;
        			scrambled[i] = temp;

        		}
        }

    if(scramOrder == 12)
       {
        d1 = 0;
       	d2 = 0;
       	d3 = 0;
       	d4 = 0;
       	d5 = 0;
       	d6 = 0;
       	d7 = 0;
       	d8 = 0;
       	d9 = 0;
       	d10 = 0;
       	d11 = 0;
       	d12 = 0;
       	for (i = 0; i < SIZE; i++)  //loop to
       		{
       			temp = xor(xor(d7, d12), bi[i]);

       			d12 = d11;
       			d11 = d10;
       			d10 = d9;
       			d9 = d8;
       			d8 = d7;
       			d7 = d6;
       			d6 = d5;
       			d5 = d4;
       			d4 = d3;
       			d3 = d2;
       			d2 = d1;
       			d1 = temp;
       			scrambled[i] = temp;

       		}
       }



/*
 	if (scrambled[i] == 1)
		{
			setGPIO(0, 2);
		}
		else
		{
			clearGPIO(0, 2);
		}
*/

	/*
										printf("\nScrambled Data\n");
										for(i=0; i<SIZE;i++)
											{
												printf("%x", scrambled[i]);
											}
	*/
	char input[SIZE]={0};
	char d[BUFF_SIZE][8];                  // input matrix obtained from input data
	k=0;
											//printf("Input data =  \n");
	for(j=0; j < (SIZE); j++ )
		{
			input[j] = scrambled[j] + '0';
											//printf("%x", input[j]);
		}
											//printf("\n D matrix =\n");
	for(i=0;i<BUFF_SIZE;i++)
		{
			for(j=0;j<8;j++)
				{
					d[i][j]=input[k];
					k++;
											//printf("%x", (d[i][j]);
				}
											//printf("\n");
		}
	int parity[paritySize]={1,1,0,0,0,1,1,0,0,0,1,1,1,0,0,1,1,0,1,0,0,1,0,1,1,1,1,0,0,1,1,1};
	//int parity[paritySize]={0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,1,1,0,0,1,0,0,0,0,0,1,0,0,1,1,1,0,0,0,0,0,0,1,1,0,0};

//-------------------------------------------------------------------------------------------------------------------------------------
//Generator matrix
	int l,sum=0;
	int paritybit=0;
	int g[Identity][(paritySize/8)+Identity];
		for(i=0;i<Identity;i++)
	{
		for(j=0;j<Identity;j++)
		{
	   if(i==j)
		  g[i][j]=1;
	    else
	  	g[i][j]=0;
	  }
	}

		for(i=0;i<Identity;i++)
	{
		for(j=8;j<((paritySize/8)+Identity);j++)
		{
		  g[i][j]=parity[paritybit];
	    paritybit++;
	  }
	}
#if 0
		for(i=0;i<Identity;i++)
	{
		for(j=0;j<((paritySize/8)+Identity);j++)
		{
	  printf("%d",g[i][j]);
	  }
	  printf("\n");
	}
#endif
//------------------------------------------------------------------------------------------------------------------------------------
//c-matrix
		int c[BUFF_SIZE][SizeOfLbcColumn];          // C is multiplication of D and G matrix
	        for(i=0;i<BUFF_SIZE;i++)
	{
		for(j=0;j<SizeOfLbcColumn;j++)
		{
			for(l=0;l<8;l++)
			{
			sum=(sum+(d[i][l]-'0')*g[l][j])%2;
			}
			c[i][j]=sum;
			sum=0;
		}
	}

#if 0
	    printf("C matrix- coding matrix(D*G) = \n");
		for(i=0;i<BUFF_SIZE;i++)
	{
		for(j=0;j<SizeOfLbcColumn;j++)
		printf("%d",c[i][j]);                    // printig C matrix
		printf("\n");
	}
	        printf("\n");
#endif
//-------------------------------------------------------------------------------------------------------------------------------------
    k=0;
    printf("C matrix- coding matrix(D*G) = \n");
    for(i=0;i<BUFF_SIZE;i++)
    	{
    		for(j=0;j<SizeOfLbcColumn;j++)
    			{
    				C[k] = c[i][j];
    				k=k+1;
    											//printf("%d",c[i][j]);                    // printig C matrix
    			}
    									        //printf("\n");
    	}


//while(1)
//{
   // initTimer0(timer0,100000,1000,callback_timer);
    //enableTimer0();
    //status =0;
    //printf("\n");
    /*
    for( j=0;j<16;j++)
      	{
      		for(i=7;i>=0;i--)
      			{
      				tx = ((sync[j]&(1<<i))>>i);
      				status =1;
      				if (tx == 1)
      			{
      				setGPIO(0, 2);
      				printf("1");
      			}
      				else
      			{
      				clearGPIO(0, 2);
      				printf("0");
      			}
      				while(status);
      			}
      	}
*/
#if 0
    for( j=0;j<lisa;j++)
    	{
    		for(i=7;i>=0;i--)
    			{
    				tx = ((sync[j]&(1<<i))>>i);
    				status =1;
    				if (tx == 1)
    			{
    				setGPIO(0, 2);
    										//printf("1");
    			}
    				else
    			{
    				clearGPIO(0, 2);
    										//printf("0");
    			}
   // 				while(status);
    				delay(180);
    			}
    	}
#endif

    										//printf("\n");
    for(j=0;j<(BUFF_SIZE*SizeOfLbcColumn);j++)
		{
				status =1;
     			if (C[j] == 1)
    		{
    			setGPIO(0, 2);
     												printf("1");
    		}
     			else
    		{
    			clearGPIO(0, 2);
    												printf("0");
    		}
     			//while(status);
     			delay(180);
		}
#if 0
    paritybit=0;
    int h[(paritySize/8)+Identity][(paritySize/8)];
    	for(i=0;i<8;i++)
    {
    	for(j=0;j<(paritySize/8);j++)
    	{
        h[i][j]=parity[paritybit];
        paritybit= paritybit+1;
      }
    }


    	for(i=8;i<((paritySize/8)+Identity);i++)
    {
    	for(j=0;j<(paritySize/8);j++)
    	{
       if((i-8)==j)
    	  h[i][j]=1;
        else
      	h[i][j]=0;
      }
    }

    printf("H matrix = \n");
    	for(i=0;i<((paritySize/8)+Identity);i++)
    {
    	for(j=0;j<(paritySize/8);j++)
    	{
      printf("%d",h[i][j]);
      }
      printf("\n");
    }



    	int s[BUFF_SIZE][(SizeOfLbcColumn-8)];          // S is multiplication of c and h matrix
    sum=0;
            for(i=0;i<BUFF_SIZE;i++)
    {
    	for(j=0;j<(SizeOfLbcColumn-8);j++)
    	{
    		for(l=0;l<SizeOfLbcColumn;l++)
    		{
    		sum=(sum+(c[i][l]-'0')*h[l][j])%2;
    		}
    		s[i][j]=sum;
    		sum=0;
    	}
    }

    	printf("S_dash matrix- syndrom word without noise i.e E ( C*h ) = \n");
    	for(i=0;i<BUFprintfiF_SIZE;i++)
    {
    	for(j=0;j<(SizeOfLbcColumn-8);j++)
    	printf("%d",s[i][j]);                    // printig C matrix
    	printf("\n");
    }
            printf("\n");

#endif



#if 0
    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        i++ ;
    }

#endif
    return 0 ;

}
