/****************************************************************************
 *   $Id:: extint.c 5670 2010-11-19 01:33:16Z usb00423                      $
 *   Project: NXP LPC17xx EINT example
 *
 *   Description:
 *     This file contains EINT code example which include EINT
 *     initialization, EINT interrupt handler, and APIs for EINT.
 *
 ****************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
 ****************************************************************************/

#include "LPC17xx.h"
#include "type.h"
#include "extint.h"
#include "timer.h"
#include "uart.h"
#include <time.h>
#include <stdio.h>
#include <string.h>
#include "timer.h"

#define sizeoflisa 8
#define BUFF_SIZE 18
#define LBC_COL 14
//#define LBC_COL 14
#define BI12_SIZE BUFF_SIZE*LBC_COL
#define BI8_SIZE BUFF_SIZE*8
#define RBSIZE (sizeoflisa*8 + BI12_SIZE)
#define delayused 5

unsigned char rx;
unsigned char status =0;

volatile uint32_t eint0_counter;
extern uint32_t timer0_m0_counter, timer1_m0_counter;
extern uint32_t timer0_m1_counter, timer1_m1_counter;
int resultBuf[4];

/*
void delay(int ms)
{
	int multiplier = 24000,count;
	for ( count = multiplier*ms ; count >0  ; count--);

}

*/

void delay(uint32_t delayInMs)
{
	LPC_TIM0->TCR = 0x02;		/* reset timer */
	LPC_TIM0->PR  = 0x00;		/* set prescaler to zero */
	LPC_TIM0->MR0 = delayInMs * (9000000 / 1000-1);
	LPC_TIM0->IR  = 0xff;		/* reset all interrrupts */
	LPC_TIM0->MCR = 0x04;		/* stop timer on match */
	LPC_TIM0->TCR = 0x01;		/* start timer */

	/* wait until delay time has elapsed */
	while (LPC_TIM0->TCR & 0x01);

	return;
}

int xor(int a, int b)   //xor function
{
	if (a == b)  // a = 0 , b = 0 or a = 1, b = 1
	{
		return 0;
	}
	else if (a == 1)  //a = 1, b = 0
	{
		return 1;
	}
	else if (b == 1)  // a = 0, b = 1
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void callback_timer(void)
{
	//printf("%d\n",tx);
//	if(rx == 1)
	//{
		//printf("1 ");
	//}
	//else
	//{
	//	printf("0 ");
	//}
	status = 0;
}

void EINT0_IRQHandler (void)
{
	int i = 0,j = 0, y,z;
	unsigned char buffer[BI8_SIZE] = {0};
	unsigned char temp = 0;
	unsigned char bufferrx[BI12_SIZE]={0};

	unsigned char lbcArray[BUFF_SIZE][12]={0};
	unsigned char dataArray[BUFF_SIZE][8]={0};
	unsigned char bufferLBC[BI8_SIZE]={0};
    unsigned char descram[BI8_SIZE]={0};
	unsigned char outArray[BUFF_SIZE];

	int d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12;
	unsigned char temp2;
	int scramOrder,lbcOrder;
	int hex = 0;
	unsigned char rb[RBSIZE]={0};



	volatile static int k = 0;
    LPC_SC->EXTINT = EINT0;        /* clear interrupt */
    printf("\n=====interrupt=======\n");
     if(k == 0)
    {
        k++;
        LPC_GPIOINT->IO2IntClr = 0xFFFFFFFF;
        LPC_GPIOINT->IO0IntClr = 0xFFFFFFFF;
        NVIC_ClearPendingIRQ(EINT0_IRQn);
        return;
    }
    LPC_GPIOINT->IO2IntEnR &= ~((0x01 <<10));
    NVIC_DisableIRQ(EINT0_IRQn);
    NVIC_ClearPendingIRQ(EINT0_IRQn);
    //delay(190);

    for(i = 0; i < RBSIZE; i++)	// size of the data in bytes
       {
           if(((LPC_GPIO2->FIOPIN)&(1 <<10)))		//if(LPC_GPIO2 ->FIOPIN & (1 <<10))
            {
                temp = 1;
            }
            else
            {
                temp = 0;
            }
            delay(delayused);
         rb[i] = temp;
        }

       //printf(" %c\n",rb[i]);


	//unsigned char hex[32]={0xAA,'h','o',};

/*	GPIOinitOut(0, 2);
    initTimer0(timer0,100000,1000,callback_timer);
    //enableTimer0();

    status =0;
    for( j=0;j<16;j++)
    {
    	for(i=7;i>=0;i--)
          {
                  if(((LPC_GPIO2->FIOPIN)&(1 <<10)))
                  {
          		  rx = 1;
                  printf("i am at 1 \n");
                  }
                  else
                  {
                	  printf("i am at 0\n");
                  rx = 0;
                  }
          				status =1;
          				if (rx == 1)
          			{
          				printf("check if equal to 1\n");
          			}
          				else
          			{
          				printf("check if equal to  0\n");
          			}
          				printf("before while\n");
          				while(status);
          	}
    }

*/
   /* for(i = 0; i < BUFF_SIZE; i++)
        {
            temp = 0;
            for(j = 7; j >=0 ; j--)
            {
               if(((LPC_GPIO2->FIOPIN)&(1 <<10)))		//if(LPC_GPIO2 ->FIOPIN & (1 <<10))
                {
                    temp |= (1<< j);
                }
                else
                {
                    temp &= ~(1 << j);
                }
                delay(56);
            }
            buffer[i] = temp;
           //printf(" %c\n",buffer[i]);
       }

*/


     /*  initTimer0(timer0,100000,100,callback_timer);
        enableTimer0();
        status = 0;
        for(j = 0; j<3; j++)
        {
        	//for(i=0;i<8;i++)
            for(i=7;i>=0;i--)
            {
               	//rx = ((LPC_GPIO2->FIOPIN)&(1 <<10));
            	rx = 1;
            	status=1;
            	if (rx == 1)
            	{
            		printf("1");
            	}
            	else
                {
            	   printf("0");
            	}
            	while(status);
            }
        }

      for(i = 0; i < (BI12_SIZE + 128); i++)
      {
    	  printf("%d ",rb[i]);
      }

    for(i = 0; i < BUFF_SIZE; i++)
    {
        temp = 0;
        for(j = 7; j >=0 ; j--)
        {
           	if(LPC_GPIO2 ->FIOPIN & (1 <<10))
            {
                temp |= (1<< j);
            }
            else
            {
                temp &= ~(1 << j);
            }
            delay(56);
        }
        rb[i] = temp;
       //printf(" %c\n",rb[i]);
   }

   	i = 0;
   	for (y = 0; y<BUFF_SIZE; y++)
   	{
   		for (z = 7; z >=0; z--)			// Hex to binary conversion
   		{
   			if(((buffer[y] >> z) & 1) != 0 )
   			{
   				rb[i] = 1;
   			}
   			else
   			{
   				rb[i] = 0;
   			}
   			i++;
   		}
   	}*/

   	int syncbit,temporaryindex,index;
   		syncbit=0;
   		//unsigned char bufferrx[BI12_SIZE]={0};
   	int bufloop=0;
   	//unsigned char rb[RBSIZE] ={1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,1,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,1,1,0,1,0,0,1,0,0,1,0,1,0,0,1,0,1,1,0,1,0,0,1,1,0,1,0,1,0,0,1,1,1,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,1,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,0,1,0,1,1,0,0,1,0,1,0,1,1,0,1,1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,1,1,0,1,1,0,0,1,1,1,1,1,1,1,0,1,0,0,0,0,1,1,0,0,0,0,1,0,0,0,1,1,1,1,0,1,0,1,0,1,0,0,1,0,1,1,1,0,1,1,1,1,1,1,1,0,0,1,1,1,1,0,0,0,0,1,1,1,0,0,0,0,1,1,0,0,0,1,1,1,1,1,0,1,0,1,1,0,0,0,1,0,0,1,1,1,1,1,1,0,1,0,1,0,0,0,1,1,0,0,1,0,0,1,0,1,0,0,0,1,0,0,1,1,0,1,1,0,0,1,0,1,0,1,0,0,1,0,1,1,1,1,0,0,1,0,0,1,0,1,0,0,0,1,0,0,1,0,1,1,0,0,1,0,1,0,1,1,1,1,0,1,0,0,0,0,0,1,1,1,1,0,0,1,1,0,1,0,1,1,0,0,0,0,0,1,1,1,0,1,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,0};
   	// unsigned char rb[rbsize] ={1,0,1,1,0,0,1,1,1,1,1,1,1,0,1,0,0,0,0,1,1,0,0,0,0,1,0,0,0,1,1,1,1,0,1,0,1,0,1,0,0,1,0,1,1,1,0,1,1,1,1,1,1,1,0,0,1,1,1,1,0,0,0,0,1,1,1,0,0,0,0,1,1,0,0,0,1,1,1,1,1,0,1,0,1,1,0,0,0,1,0,0,1,1,1,1,1,1,0,1,0,1,0,0,0,1,1,0,0,1,0,0,1,0,1,0,0,0,1,0,0,1,1,0,1,1,0,0,1,0,1,0,1,0,0,1,0,1,1,1,1,0,0,1,0,0,1,0,1,0,0,0,1,0,0,1,0,1,1,0,0,1,0,1,0,1,1,1,1,0,1,0,0,0,0,0,1,1,1,1,0,0,1,1,0,1,0,1,1,0,0,0,0,0,1,1,1,0,1,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,0
   	//};
   	int done;
   		lable1: done=1;
   		while(done==1)
   		{
   		if( (rb[syncbit]==1) && (rb[syncbit+1]==0) && (rb[syncbit+2]==1) && (rb[syncbit+3]==0) )
   	  	{
   	    done = 0;
   	  }
   	else
   	  {
   	  syncbit = syncbit+ 1;
   	  }
   	}
   	//for(j=1;j<3;j++)
   	j=1;
   	if( ( rb[syncbit] == rb[syncbit + (8*j)] ) && ( rb[syncbit+1] == rb[(syncbit+1) + (8*j)] ) && ( rb[syncbit+2] == rb[(syncbit+2) + (8*j)] ) && ( rb[syncbit+3] == rb[(syncbit+3) + (8*j)] ) )
   	  {
   	    if( ( ( ( rb[syncbit+4]*8 ) + ( rb[syncbit+5]*4 ) + ( rb[syncbit+6]*2 ) + ( rb[syncbit+7]*1 ) +1 ) ==  ( ( rb[syncbit+12]*8 ) + ( rb[syncbit+13]*4 ) + ( rb[syncbit+14]*2 ) + ( rb[syncbit+15]*1 ) ) ) && ( ( ( rb[syncbit+4]*8 ) + ( rb[syncbit+5]*4 ) + ( rb[syncbit+6]*2 ) + ( rb[syncbit+7]*1 ) +2 ) ==  ( ( rb[syncbit+20]*8 ) + ( rb[syncbit+21]*4 ) + ( rb[syncbit+22]*2 ) + ( rb[syncbit+23]*1 ) ) ) )
   	      {
   	        temporaryindex = ( ( rb[syncbit+4]*8 ) + ( rb[syncbit+5]*4 ) + ( rb[syncbit+6]*2 ) + ( rb[syncbit+7]*1 )  );
   	        index = ( syncbit + ( (sizeoflisa-temporaryindex) * 8 ) );
   	        printf("payload\n");

   	        for(i=index; i<RBSIZE;i++)
   	        {
   	        	//printf("%d",rb[i]);
   		bufferrx[bufloop] = rb[i];
   													printf("%d",bufferrx[bufloop]);
   		bufloop = bufloop + 1;
   		}
   	      }
   	    else
   	    {
   	      syncbit = syncbit + 1;
   	      goto lable1;
   	    }
   	  }
   	else
   	  {
   	    syncbit = syncbit + 1;
   	    goto lable1;
   	  }




 printf("\nEnter LBC Column Size(12 or 14): ");
 scanf("%d", &lbcOrder);

   	//LBC

   		for(i=0; i<BUFF_SIZE; i++)
   		{
   			for(j=0; j<LBC_COL;j++)
   			{
   				lbcArray[i][j] = bufferrx[i*LBC_COL+j];
   			}
   		}

   		printf("Data Received:\n");
   		for(i=0; i<BUFF_SIZE; i++)
   		{
   			for(j=0; j<LBC_COL; j++)
   			{
   				printf("%d ", lbcArray[i][j]);
   			}
   			printf("\n");
   		}


   		for(i=0; i<BUFF_SIZE; i++)
   		{
   			for(j=0; j<8; j++)
   			{
   				dataArray[i][j] = lbcArray[i][j];
   			}
   		}
#if 0
   	        printf("LBC decoding:\n");
   		for(i=0; i<BUFF_SIZE; i++)
   		{
   			for(j=0; j<8; j++)
   			{
   				printf("%d ", dataArray[i][j]);
   			}
   			printf("\n");
   		}
#endif
   		for(i=0; i<BUFF_SIZE; i++)
   		{
   			for(j=0; j<8; j++)
   			{
   				bufferLBC[i*8+j] = dataArray[i][j];
   			}
   		}

#if 0
   		printf("Data Array:\n");
   		for(i=0; i<BI8_SIZE; i++)
   		{
   			printf("%d ", bufferLBC[i]);
   		}
#endif
   	//Descrambling

   	    d1 = 0;
   		d2 = 0;
   		d3 = 0;
   		d4 = 0;
   		d5 = 0;
   		d6 = 0;
   		d7 = 0;
   		d8 = 0;
   	 	d9 = 0;
   	 	d10 = 0;
   	 	d11 = 0;
   	 	d12 = 0;

   		printf("\nEnter Scrambler Order(5, 7, 9 or 12): ");
   		scanf("%d", &scramOrder);


   	        if(scramOrder == 5)
   		{
   			for(i = 0; i<BI8_SIZE; i++)
   			{
   				temp2 = xor(xor(d3,d5), bufferLBC[i]);

   				d5 = d4;
   				d4 = d3;
   				d3 = d2;
   				d2 = d1;
   				d1 = bufferLBC[i];

   				descram[i] = temp2;
   			}

   		}

   		if(scramOrder == 7)
   	        {
   			for(i = 0; i<BI8_SIZE; i++)
   			{
   				temp2 = xor(xor(d4,d7), bufferLBC[i]);

   				d7 = d6;
   				d6 = d5;
   				d5 = d4;
   				d4 = d3;
   				d3 = d2;
   				d2 = d1;
   				d1 = bufferLBC[i];

   				descram[i] = temp2;
   												//printf("%d ", descram[i]);
   			}

   		}
   		if(scramOrder == 9)
   		{
   		for(i = 0; i<BI8_SIZE; i++)
   			{
   				temp2 = xor(xor(d5,d9), bufferLBC[i]);

   				d9 = d8;
   			    d8 = d7;
   				d7 = d6;
   				d6 = d5;
   				d5 = d4;
   				d4 = d3;
   				d3 = d2;
   				d2 = d1;
   				d1 = bufferLBC[i];

   				descram[i] = temp2;
   																	//printf("%d ", descram[i]);
   			}
   		}


   		if(scramOrder == 12)
   	   		{
   	   		for(i = 0; i<BI8_SIZE; i++)
   	   			{
   	   				temp2 = xor(xor(d7,d12), bufferLBC[i]);

   	   				d12 = d11;
   	   				d11 = d10;
   	   				d10 = d9;
   	   				d9 = d8;
   	   			    d8 = d7;
   	   				d7 = d6;
   	   				d6 = d5;
   	   				d5 = d4;
   	   				d4 = d3;
   	   				d3 = d2;
   	   				d2 = d1;
   	   				d1 = bufferLBC[i];

   	   				descram[i] = temp2;
   	   																	//printf("%d ", descram[i]);
   	   			}
   	   		}

   		printf("DeScrambled data: \n");
   	   	for (i = 0; i < BI8_SIZE; i++)
   	   	{
   	   		printf("%d", descram[i]);
   	   	}
   	   	printf("\n");


   		//binary to hex and print the message


   	    for (j = 0; j < sizeof(descram) / 8; ++j)
   	    {
   	       hex = 0;
   	       for (i = j * 8; i < (j + 1) * 8; i += 4)
   	      {
   	       int start = i;
   	       long int temp3 = 0;
   	       while (start < i + 4)
   	       {
   	        temp3 *= 2;
   	        temp3 += descram[start++] - 0;
   	       }
   	       hex *= 16;
   	       hex += temp3;
   	    }
   	       outArray[j] = hex;
   	      // printf("Equivalent hexadecimal value: %lX \n", hexadecimalval);
   	    }
   	    for (j = 0; j < sizeof(descram) / 8; ++j)
   	    printf("%c ", outArray[j]);

   	    //printf("%lx ",(long int) outArray[j]);





 /*  	int d1, d2, d3, d4, d5;
   	//int buffernew[BI_SIZE] = { 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0 };
   	int descram[BI8_SIZE] = { 0 };
   	int tempe;

   	printf("Data Received:\n");
   	for (i = 0; i < BI_SIZE; i++)
   	{
   		printf("%d", bufferrx[i]);
   	}
   		printf("\n");

   		// DeScrambler
   		d1 = 0;
   		d2 = 0;
   		d3 = 0;
   		d4 = 0;
   		d5 = 0;

   		for (i = 0; i < BI_SIZE; i++)
   		{
   			tempe = xor(xor(d3, d5), bufferrx[i]); //calculate descrambler values

   			d5 = d4;
   			d4 = d3;
   			d3 = d2;
   			d2 = d1;
   			d1 = bufferrx[i];

   			descram[i] = tempe;
   		}

   		printf("DeScrambled data: \n");
   		for (i = 0; i < BI_SIZE; i++)
   		{
   			printf("%d", descram[i]);
   		}
   		printf("\n");

   		//int descram[40] = { 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1 };

   		int hex = 0;
   		//int j = 0;
   		int array[BUFF_SIZE];

   		for (j = 0; j < sizeof(descram)/32 ; ++j)
   		{
   			hex = 0;
   			for (i = j * 8; i < (j + 1) * 8; i += 4)
   			{
   				int start = i;
   				int temp = 0;
   				while (start < i + 4)
   				{
   					temp *= 2;
   					temp += descram[start++]-0;
   				}
   				hex *= 16;
   				hex += temp;
   			}
   			array[j] = hex;
   		}

   		printf("\nMessage Received\n");
   		for (j = 0; j < sizeof(descram) / 32; ++j)
   			printf("%c ", (int)array[j]);



*/


  /* 	long int hexadecimalval = 0, d = 0, remainder;
   	    int e = 0;
   	    unsigned char array[512];
   	    for (e = 0; e < sizeof(buffernew) / 8; ++e)
   	    {
   	       hexadecimalval = 0;
   	       for (d = e * 8; d < (e + 1) * 8; d += 4)
   	      {
   	       int start = d;
   	       long int temp = 0;
   	       while (start < d + 4)
   	       {
   	        temp *= 2;
   	        temp += buffernew[start++] - 0;
   	       }
   	       hexadecimalval *= 16;
   	       hexadecimalval += temp;
   	    }
   	       array[j] = hexadecimalval;
   	      // printf("Equivalent hexadecimal value: %lX \n", hexadecimalval);
   	    }
   	    for (e = 0; e < sizeof(buffernew) / 8; ++e)
   	    printf("%c ", (unsigned char)array[e]);
*/
    LPC_GPIOINT->IO2IntEnR = ((0x01 <<10));
    LPC_GPIOINT->IO2IntClr = 0xFFFFFFFF;
    LPC_GPIOINT->IO0IntClr = 0xFFFFFFFF;
}
uint32_t EINTInit( void )
{
    LPC_PINCON->PINSEL4 &= ~(3 << 20 );    /* set P2.10 as EINT0 */	/* set P2.10 as EINT0 and P2.0~7 GPIO output */ /*select pin number 20:21 as 01 to enable external intrupt */
    LPC_PINCON->PINSEL4 |= (1 << 20 );    /* set P2.10 as EINT0 */
    LPC_PINCON->PINMODE4 = 0;						// for making pull-up use 00
    LPC_GPIOINT->IO2IntEnR |= ((0x01 <<10));				/* Port2.10 is rising edge. */
    LPC_GPIOINT->IO2IntEnF &= ~((0x01 <<10));
    LPC_SC->EXTMODE = EINT0_EDGE | EINT3_EDGE ;        /* INT0 edge trigger */
    LPC_SC->EXTPOLAR |= (1<< 0 | 1 << 3);                /* INT0 is falling edge by default */
    NVIC_EnableIRQ(EINT0_IRQn);
    return 0;
}

/******************************************************************************
 **                            End Of File
******************************************************************************/
