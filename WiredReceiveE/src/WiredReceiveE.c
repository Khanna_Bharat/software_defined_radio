/*
===================================================================================
 Name        : Receive
 Author      : Bharat Khanna
 Version     :
 Copyright   : $(copyright)
 Description : Receiving data, descrambe the data, and implement the LISA Algorithm
               display the message received
====================================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif
#include <cr_section_macros.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "extint.h"
#include "timer.h"
/*
#define BUFF_SIZE 18
#define BI12_SIZE BUFF_SIZE*12
#define BI8_SIZE BUFF_SIZE*8
#define rbsize 344
*/


void GPIOinitOut(uint8_t portNum, uint32_t pinNum)	// Initilize output ports and pins
{
	if (portNum == 0)  //Initilize port 0
	{
		LPC_GPIO0->FIODIR |= (1 << pinNum);
	}
	else
	{
		puts("Not a valid port!\n");
	}
}

void setGPIO(uint8_t portNum, uint32_t pinNum)	 // set GPIO when set output High
{
	if (portNum == 0) //set port 0
	{
		LPC_GPIO0->FIOSET = (1 << pinNum);
	}
	//Can be used to set pins on other ports for future modification
	else
	{
		puts("Only port 0 is used, try again!\n");
	}
}


void clearGPIO(uint8_t portNum, uint32_t pinNum)	// clear GPIO when clear output Low
{
	if (portNum == 0)
	{
		LPC_GPIO0->FIOCLR = (1 << pinNum);
	}
	else
	{
		puts("Only port 0 is used, try again!\n");
	}
}

void GPIOinitIN(uint8_t portNum, uint32_t pinNum)			// initilize input ports and pin
{
	if (portNum == 2)
	{
		LPC_GPIO2->FIODIR &= ~(1 << pinNum);
	}
	else
	{
		puts("Not a valid port!\n");
	}
}



int main()
{
GPIOinitOut(0,2);		//Initialize port 0.2 as output
	GPIOinitIN(2,10);		//Set pin 2.10 as Input
	//enableTimer0();
	EINTInit();				// calling of a interrupt function
	while(1); 				//infinite loop
	return -1;
}
